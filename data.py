import pandas as pd
import numpy as np
import csv


def read_data(file='./data/data.csv'):
    df = pd.read_csv(file, sep='|')
    return df


def add_label(s):
    # very basic cleaning of labels to make it more legible
    if ':' in s:
        s = s.split(':')[0]
    if '(' in s:
        s = s.split('(')[0]
    return '__label__' + s.lower().replace(' ', '-')


def get_labels(df, column='category_label', none_label='__label__none'):
    '''
    set the data to be read
    '''
    # replace all empty with label
    df = df.where((pd.notnull(df)), none_label)
    for idx, row in df[column].iteritems():
        if row != none_label:
            if '*' in row:
                row = ' '.join([add_label(r) for r in row.split('*')])
            else:
                row = add_label(row)
            df.loc[idx, column] = row
    return df


def save_df(df, columns=['category_label', 'content']):
    '''
    couldnt figure out how to do this in just pandas
    '''
    df = df[columns].values
    with open('data/labeled_data.txt', 'w') as f:
        for line in df:
            f.write(f"{line[0]} {line[1].lower()}\n")

if __name__=='__main__':
    df = read_data()
    df = get_labels(df)
    save_df(df)
