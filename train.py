import fastText
from fastText import train_supervised


class Model:
    def __init__(self, train_data, pretrained=None):
        if pretrained:
            pass
        self.train_data = train_data

    def train(self):
        self.model = train_supervised(
            self.train_data,
            epoch=100,
            wordNgrams=2,
            loss="softmax",
            pretrainedVectors="./data/pretrained/crawl-300d-2M.vec")

    def create_ft_matrix(self, ft_matrix=None):
        self.ft_words = self.model.get_words()
        self.word_frequencies = dict(zip(*self.model.get_words(include_freq=True)))
        self.ft_matrix = ft_matrix
        if self.ft_matrix is None:
            self.ft_matrix = np.empty((len(self.ft_words), self.model.get_dimension()))
            for i, word in enumerate(self.ft_words):
                self.ft_matrix[i, :] = self.model.get_word_vector(word)

    def find_nearest_neighbor(self, query, vectors, n=10,  cossims=None):
        if cossims is None:
            cossims = np.matmul(vectors, query, out=cossims)

        norms = np.sqrt((query**2).sum() * (vectors**2).sum(axis=1))
        cossims = cossims/norms
        result_i = np.argpartition(-cossims, range(n+1))[1:n+1]
        return list(zip(result_i, cossims[result_i]))

    def nearest_words(self, word, n=10, word_freq=None):
        result = self.find_nearest_neighbor(
            self.model.get_word_vector(word), self.ft_matrix, n=n)
        if word_freq:
            return [(self.ft_words[r[0]], r[1]) for r in result if self.word_frequencies[self.ft_words[r[0]]] >= word_freq]
        else:
            return [(self.ft_words[r[0]], r[1]) for r in result]
        pass

    def predict_sentence(self, sent, n=5):
        return self.model.predict(sent, k = n)


if __name__ == '__main__':
    model = Model('data/labeled_data.txt')
    model.train()
    print(model.predict_sentence('im super upset this is bullshit'))
